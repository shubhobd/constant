<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
                                    class base_class
                            {
                                function say_a()
                                {
                                    echo "'a' - said the " . __CLASS__ . "<br/>";
                                }

                                function say_b()
                                {
                                    echo "'b' - said the " . get_class($this) . "<br/>";
                                }

                            }

                            class derived_class extends base_class
                            {
                                function say_a()
                                {
                                    parent::say_a();
                                    echo "'a' - said the " . __CLASS__ . "<br/>";
                                }

                                function say_b()
                                {
                                    parent::say_b();
                                    echo "'b' - said the " . get_class($this) . "<br/>";
                                }
                            }

                            $obj_b = new derived_class();

                            $obj_b->say_a();
                            echo "<br/>";
                            $obj_b->say_b();
?>
    </body>
</html>
